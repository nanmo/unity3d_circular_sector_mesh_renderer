.. -*- restructuredtext -*-

===========================
CircularSectorMeshRenderer
===========================

About
-------
Dynamic Generating Circular Sector Mesh

License
--------
BSD 3-Clause

Sample
---------
http://bleble.s321.xrea.com/data/circularsectormeshsample/circularsectormeshsample.html


(C)nanmo