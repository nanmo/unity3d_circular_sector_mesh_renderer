using UnityEngine;
using System.Collections;

public class SampleGUI : MonoBehaviour {
	public CircularSectorMeshRenderer target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI () {
		GUILayout.BeginVertical( GUILayout.Width(Screen.width) );
		GUILayout.Label("DEGREE: " + target.degree.ToString() );
		target.degree = GUILayout.HorizontalSlider( target.degree, 0.0f, 360);
		GUILayout.Label("INTERVAL DEGREE " + target.intervalDegree.ToString() );
		target.intervalDegree = GUILayout.HorizontalSlider( target.intervalDegree, 0.1f, 90);
		GUILayout.Label("BEGIN OFFSET DEGREE: " + target.beginOffsetDegree.ToString() );
		target.beginOffsetDegree = GUILayout.HorizontalSlider( target.beginOffsetDegree, 0.0f, 360);
		GUILayout.Label("RADIUS: " + target.radius.ToString() );
		target.radius = GUILayout.HorizontalSlider( target.radius, 1, 100 );
		GUILayout.EndVertical();
	}
}
